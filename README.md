Patching CRDA and regulatory.bin
========================================

The Central Regulatory Domain Agent for Linux uses the regulatory.bin regulatory database for enforcing which frequencies can be used by WLAN cards. Since some Atheros cards do not allow for the regulatory domain to be changed by iw (iw reg set) we create our own regulatory.bin, substituting the regulatory info for the country where the card is stuck at (e.g. US) with german spectrum restrictions.


Easy
----
The easy approach is trying to install the patched version I compiled of both regulatory.bin and the crda executables (US <-- DE):

 * pacman -S git python2 python2-m2crypto 

 * git clone https://bitbucket.org/pfaaj/agvs-crda-gitlab/src/master/
 
 * mkdir /usr/lib/crda  OR mv /usr/lib/crda/regulatory.bin /usr/lib/crda/regulatoryOld.bin (for backing up an existing one)

 * cd agvs-crda && cp regulatory.bin /usr/lib/crda/regulatory.bin

 * make install 

 * reboot 

Hard
----

The hard approach is compiling everything

### BUILD REQUIREMENTS


The package build requirements are:

 * python 2.7: future, swig and the m2crypto package (python-m2crypto)
 * libgcrypt or libssl (openssl) header files
 * nl library and header files (libnl1 and libnl-dev)
   available at git://git.kernel.org/pub/scm/libs/netlink/libnl.git

 * Get latest versions of the regulatory database wireless-regdb and crda:

    * git clone git://git.kernel.org/pub/scm/linux/kernel/git/sforshee/wireless-regdb.git

    * git clone git://git.kernel.org/pub/scm/linux/kernel/git/mcgrof/crda.git


If all build requirements are fullfilled  follow the instructions below:

  * Compile db.txt where US was switched by DE's spectrum restrictions (already provided in this repository): 
    *  cd wireless-regdb folder.
    *  make
  * cp regulatory.bin /usr/lib/crda/
  * cd crda/ && rm linville.key.pub.pem
  * cp /root/.wireless-regdb-root.key.priv.pem crda/pubkeys/linville.key.pub.pem
  * Compile crda and install it:
    *  make
    *  make install
    *  reboot

Source of instructions:

[Ausländische WLan-Karte mit deutschen Kanälen betreiben](https://wiki.debianforum.de/Ausl%C3%A4ndische_WLan-Karte_mit_deutschen_Kan%C3%A4len_betreiben)


Did it work?
===========


Use iwlist to check whether channels 12 and 13 are available:

* iwlist wlp5s0mon frequency


If everything worked we should see this output:

           Channel 01 : 2.412 GHz
           Channel 02 : 2.417 GHz
           Channel 03 : 2.422 GHz
           Channel 04 : 2.427 GHz
           Channel 05 : 2.432 GHz
           Channel 06 : 2.437 GHz
           Channel 07 : 2.442 GHz
           Channel 08 : 2.447 GHz
           Channel 09 : 2.452 GHz
           Channel 10 : 2.457 GHz
           Channel 11 : 2.462 GHz
           Channel 12 : 2.467 GHz
           Channel 13 : 2.472 GHz
           



