#!/bin/bash

# exit if any command fails
set -e

echo "Patching regulatory db..."

pacman -S git python2 python2-m2crypto

git clone https://bitbucket.org/pfaaj/agvs-crda-gitlab/src/master/

mkdir -p /usr/lib/crda 

cd agvs-crda && cp regulatory.bin /usr/lib/crda/regulatory.bin

make install

echo "Database was patched! Please reboot now..."

